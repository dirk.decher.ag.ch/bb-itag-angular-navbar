import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IsMenuVisibleMethod, IMenuItem, IBaseMenuInfo } from '../model';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'lib-master-menu-button',
  templateUrl: './master-menu-button.component.html',
  styleUrls: ['./master-menu-button.component.css']
})
export class MasterMenuButtonComponent {

  public faChevronDown = faChevronDown;

  @Input()
  public menuItem: IMenuItem;

  @Input()
  public isMenuVisibleMethod: IsMenuVisibleMethod;

  @Input()
  public location: 'top' | 'left' = 'top';

  @Input()
  public routerLinkActiveClass = 'active';

  @Output()
  public menuClicked: EventEmitter<IBaseMenuInfo> = new EventEmitter<IBaseMenuInfo>();

  public onMenuClicked( baseMenuInfo: IBaseMenuInfo) {
    this.menuClicked.emit(baseMenuInfo);
  }

  public getActiveRouterLink() {
    if (! this.menuItem) { return ''; }
    if (! this.menuItem.routerLink || this.menuItem.routerLink.length === 0) { return ''; }
    if ( this.routerLinkActiveClass && this.routerLinkActiveClass.length > 0 ) { return this.routerLinkActiveClass; }
    return '';
  }

}
