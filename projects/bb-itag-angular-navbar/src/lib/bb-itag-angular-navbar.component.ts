import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { MatSidenav } from '@angular/material/sidenav';
import { ThemePalette } from '@angular/material/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { faBars, faUser } from '@fortawesome/free-solid-svg-icons';
import { IBaseMenuInfo, IMenuConfiguration, IMenuItem, IProfileMenuItem, IsMenuVisibleMethod } from './model';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'lib-bb-itag-angular-navbar',
  templateUrl: './bb-itag-angular-navbar.component.html',
  styleUrls: ['./bb-itag-angular-navbar.component.css']
})
export class BbItagAngularNavbarComponent {


  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router,
  ) {
    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        // console.dir(event);
        // close menu
        this.isOpened = false;
        this.sideNav.opened = false;
      }
    });

  }

  @Input ()
  public set isMenuVisibleMethod(value: IsMenuVisibleMethod) {
    this.menuConfiguration.isMenuVisibleMethod = value;
  }
  @Input ()
  public set color(value: ThemePalette) {
    this.menuConfiguration.color = value;
  }
  @Input ()
  public set leftMenuItems(value: IMenuItem[]) {
    this.menuConfiguration.leftMenuItems = value;
  }
  @Input ()
  public set rightMenuItems(value: IMenuItem[]) {
    this.menuConfiguration.rightMenuItems = value;
  }
  @Input ()
  public set applicationName(value: string) {
    this.menuConfiguration.applicationName = value;
  }
  @Input ()
  public set applicationLogoAlt(value: string) {
    this.menuConfiguration.applicationLogoAlt = value;
  }
  @Input ()
  public set applicationLogoUri(value: string) {
    this.menuConfiguration.applicationLogoUri = value;
  }
  @Input ()
  public set profile(value: IProfileMenuItem) {
    this.menuConfiguration.profile = value;
  }
  @Input ()
  public set activeRouterLinkClass(value: string) {
    this.menuConfiguration.activeRouterLinkClass = value;
  }
  @Input ()
  public set showDebug(value: boolean) {
    this.menuConfiguration.showDebug = value;
  }
  @Input ()
  public set applicationRouterLink(value: string) {
    this.menuConfiguration.applicationRouterLink = value;
  }
  @Output ()
  public menuClicked: EventEmitter<IBaseMenuInfo> = new EventEmitter<IBaseMenuInfo>();

  @Output ()
  public hamburgerMenuStatusIsOpen: EventEmitter<boolean> = new EventEmitter<boolean>();

  @ViewChild(MatSidenav, {static: false }) sideNav: MatSidenav;

  public menuConfiguration: IMenuConfiguration = {
    isMenuVisibleMethod: this.isMenuVisibleTrueMethod,
    color: 'primary',
    leftMenuItems: [],
    rightMenuItems: [],
    applicationName: 'myApp',
    applicationLogoAlt: 'the logo of the company',
    applicationLogoUri: null,
    profile: {
      name: 'profile',
      username$: of('test'),
      isAuthenticated$: of( true ),
      showProfile: true,
      routerLink: null,

    },
    activeRouterLinkClass: 'empty',
    showDebug: false,
    applicationRouterLink: '/',
  };

  public faUser = faUser;
  public faBars = faBars;

  public isOpened = false;

  public isHandset$: Observable<boolean> = this.breakpointObserver.observe (
    [
      Breakpoints.Small,
      Breakpoints.XSmall
    ])
    .pipe (
      map ( result => result.matches ),
      shareReplay ()
    );

  private isMenuVisibleTrueMethod(menuItem: IMenuItem): Observable<boolean> {
    if ( this.menuConfiguration.showDebug ) {
      console.log ( `bb-itag-angular-navbar-component.isMenuVisibleTrueMethod.menuItem: ${ JSON.stringify ( menuItem, null, 2 ) }` );
    }
    return of ( true );
  }

  public onMenuClicked(baseMenuInfo: IBaseMenuInfo) {
    this.menuClicked.emit(baseMenuInfo);
    this.onHamburgerMenuClick(false);
  }

  public onHamburgerMenuClick( isOpened: boolean ) {
    this.isOpened = isOpened;
    this.hamburgerMenuStatusIsOpen.emit(isOpened);
  }

}
