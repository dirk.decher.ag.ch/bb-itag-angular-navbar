import { NgModule } from '@angular/core';
import { BbItagAngularNavbarComponent } from './bb-itag-angular-navbar.component';

import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MasterMenuButtonComponent } from './master-menu-button/master-menu-button.component';
import { MenuButtonComponent } from './menu-button/menu-button.component';
import {
  MatExpansionModule,
  MatListModule,
  MatMenuModule,
  MatSidenavModule,
  MatToolbarModule, MatTooltipModule,
} from '@angular/material';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatButtonModule } from '@angular/material/button';
import { FlexLayoutModule } from '@angular/flex-layout';
import { LayoutModule } from '@angular/cdk/layout';

@NgModule({
  declarations: [
    BbItagAngularNavbarComponent,
    MasterMenuButtonComponent,
    MenuButtonComponent],
  imports: [
    CommonModule,
    RouterModule,
    LayoutModule,
    FlexLayoutModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatButtonModule,
    FontAwesomeModule,
    MatMenuModule,
    MatExpansionModule,
    MatTooltipModule,
  ],
  exports: [
    BbItagAngularNavbarComponent,
  ]
})
export class BbItagAngularNavbarModule { }
