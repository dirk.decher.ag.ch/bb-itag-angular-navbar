import { Observable } from 'rxjs';
import { IBaseMenuItem } from '.';

export interface IProfileMenuItem extends IBaseMenuItem {
  showProfile: boolean;
  username$: Observable<string>;
  isAuthenticated$: Observable<boolean>;
}
