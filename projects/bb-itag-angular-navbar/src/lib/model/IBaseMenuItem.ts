import { SizeProp } from './IMenuItem';

export interface IBaseMenuInfo {
  routerLink?: string;
  tag?: string;
  name: string;
  tooltipText?: string;
}

export interface IBaseMenuItem extends  IBaseMenuInfo {
  faIcon?: any;
  faSize?: SizeProp;
}
