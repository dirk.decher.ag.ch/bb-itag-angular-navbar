import { faGrin, faRedoAlt, faSmile, faToolbox } from '@fortawesome/free-solid-svg-icons';
import { IMenuItem } from '../../projects/bb-itag-angular-navbar/src/lib/model';

const spacerIsAuthenticated: IMenuItem = {
  name: '-',
  roles: ['.'],
};
const spacerIsAdmin: IMenuItem = {
  name: '-',
  roles: ['isAdmin'],
};
const spacerIsUser1: IMenuItem = {
  name: '-',
  roles: ['isUser1'],
};
const spacerIsUser2: IMenuItem = {
  name: '-',
  roles: ['isUser2'],
};

const submenu1: IMenuItem = {
  name: 'submenu 1',
  routerLink: '/submenu1',
  roles: ['isUser1'],
  faIcon: faGrin,
};
const submenu2: IMenuItem = {
  name: 'submenu 2',
  routerLink: '/submenu2',
  faIcon: faSmile,
  faSize: '1x',
  roles: ['isUser2'],
  tooltipText: 'submenu 2 tooltip'
};
const submenu3: IMenuItem = {
  name: 'submenu 3',
  routerLink: '/submenu3',
  faIcon: faSmile,
  faSize: '1x',
  roles: ['isUser2'],
};
const menuItemAuthentication: IMenuItem = {
  name: 'Authenticated',
  roles: ['.'],
  childMenuItems: [
    submenu1,
    spacerIsAuthenticated,
    submenu2,
    submenu3
  ],
};

const menuItemSub2: IMenuItem = {
  name: 'Sub2',
  faIcon: faToolbox,
  roles: ['.'],
  childMenuItems: [
    submenu1,
    spacerIsUser1,
    submenu2,
    spacerIsUser2,
    submenu3
  ]
};

export const leftMenuItems: IMenuItem[] = [
  menuItemAuthentication,
  submenu1,
  submenu2,
  submenu3,
  menuItemSub2,
];

const menuItemAdmin: IMenuItem = {
  name: 'Admins',
  routerLink: '/admin',
  roles: ['isAdmin'],
  faIcon: faToolbox,
  faSize: '1x'
};

const menuItemRefresh: IMenuItem = {
  name: 'refresh',
  routerLink: null,
  tag: 'refresh',
  faIcon: faRedoAlt,
  roles: [],
};

export const rightMenuItems: IMenuItem[] = [
  menuItemRefresh,
  menuItemAdmin,
];



