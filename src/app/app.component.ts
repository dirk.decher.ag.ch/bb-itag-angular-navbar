/* tslint:disable:member-ordering */
import { Component } from '@angular/core';
import { ThemePalette } from '@angular/material/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { IBaseMenuInfo, IMenuItem, IProfileMenuItem, IsMenuVisibleMethod } from '../../projects/bb-itag-angular-navbar/src/lib/model';
import { leftMenuItems, rightMenuItems } from './menuConfiguration';

// @ts-ignore
import * as pjson from '../../package.json';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  private isAuthenticatedTrueMethod(): Observable<boolean> {
    console.log ( `isAuthenticatedTrueMethod` );
    return of ( true );
  }

  private static isMenuCheckedMethod( menuItem: IMenuItem ): Observable<boolean> {
    console.log(menuItem.roles);
    return of (
      menuItem.roles.indexOf ( 'isUser1' ) > -1
      || menuItem.roles.indexOf ( 'isUser2' ) > -1
      || menuItem.roles.indexOf ( 'isAdmin' ) > -1
      || menuItem.roles.indexOf ( '.' ) > -1
      || menuItem.roles.length === 0
    );
  }

  // @ts-ignore
  private profileMenuItem: IProfileMenuItem = {
    name: 'profile',
    showProfile: true,
    routerLink: '/profile',
    // faIcon: faSmile,
    // faSize: '2x'
    username$: of ( 'test' ),
    isAuthenticated$: this.isAuthenticatedTrueMethod(),
    tooltipText: 'profile tooltip'
  };

  // react on the hamburgerMenu show event
  public isHamburgerMenu = false;

  // default theme ist primary
  public color: ThemePalette = 'primary';
  // the array with the menus, showing left
  public leftMenuItems: IMenuItem[] = leftMenuItems;
  // the array with the menus, showing right
  public rightMenuItems: IMenuItem[] = rightMenuItems;
  // the name we show on the top of the menu (default: myApp)
  public applicationName: string = pjson.name;
  // we strongly recommend to add a alternate name here in case you have an uri set (default: the logo of the company)
  public applicationLogoAlt = 'the alternate name for the logo';
  // the logo to the image we show on the top left of the menu (recommended size: 120x30px)
  public applicationLogoUri = '/assets/images/logo.png';
  // how we should show the profile menu (not show by default)
  public profile: IProfileMenuItem = this.profileMenuItem;
  // what global css class we have to use to show the active menu
  public activeRouterLinkClass = 'active';
  // do we have to log some debug information like menu click, or isVisible checks
  public showDebug = true;
  // the router link on the application name / logo on the left. default /home
  public applicationRouterLink = '/profile';
  // set the property to the method to call
  public isMenuVisibleMethod: IsMenuVisibleMethod = AppComponent.isMenuCheckedMethod;

  public setColor( color: ThemePalette ) {
    this.color = color;
  }

  public onMenuClicked(menuItem: IBaseMenuInfo) {
    if (menuItem.tag) {
      alert(`tag: ${menuItem.tag}`);
    }
  }

  public onHamburgerMenuStatusIsOpen( isOpen: boolean ) {
    this.isHamburgerMenu = isOpen;
  }
}
